<?php
declare(strict_types=1);

/**
 * Cloudrexx
 *
 * @link      https://www.cloudrexx.com
 * @copyright Cloudrexx AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

namespace Cx\Modules\MediaDir\Model\Entity;

/**
 * @copyright   Cloudrexx AG
 * @author      Thomas Wirz <thomas.wirz@cloudrexx.com>
 * @package     cloudrexx
 * @subpackage  module_mediadir
 */
class WysiwygInputfieldParseTarget extends \Cx\Core_Modules\Widget\Model\Entity\WidgetParseTarget
{
    /**
     * ID of associated entry
     * @var int
     */
    protected $entryId;

    /**
     * ID of associated inputfield
     * @var int
     */
    protected $fieldId;

    /**
     * @inheritDoc
     */
    public function getWidgetContentAttributeName($widgetName) {
        return 'content';
    }

    /**
     * @return  string  Value from inputifield static::$fieldId of entry static::$entryId
     */
    public function getContent(): string {
        $entry = new \Cx\Modules\MediaDir\Controller\MediaDirectoryEntry($this->getName());
        $entry->getEntries($this->entryId, null, null, null, null, null, true);
        if (empty($entry->arrEntries[$this->entryId])) {
            return '';
        }
        $entryData = $entry->arrEntries[$this->entryId];
        $inputfields = $entry->getInputfields();
        $inputfield = new \Cx\Modules\MediaDir\Model\Entity\MediaDirectoryInputfieldWysiwyg($this->getName());
        $content = $inputfield->getRawData(
            $this->entryId,
            $inputfields[$this->fieldId], 
            $entryData['entryTranslationStatus']
        );
        return preg_replace(
            '/\\[\\[([A-Z0-9_-]+)\\]\\]/',
            '{\\1}',
            $content
        );
    }

    /**
     * @param   int $id ID of associated entry.
     */
    public function setEntryId(int $id): void {
        $this->entryId = $id;
    }

    /**
     * @param   int $id ID of associated inputfield.
     */
    public function setFieldId(int $id): void {
        $this->fieldId = $id;
    }
}
