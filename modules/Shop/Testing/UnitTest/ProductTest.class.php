<?php
/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * ProductTest
 *
 * @copyright   CLOUDREXX CMS - CLOUDREXX AG
 * @author      Thomas Wirz <thomas.wirz@cloudrexx.com>
 * @package     cloudrexx
 * @subpackage  module_shop
 */
namespace Cx\Core\Routing\Testing\UnitTest;

/**
 * ProductTest
 *
 * @copyright   CLOUDREXX CMS - CLOUDREXX AG
 * @author      Thomas Wirz <thomas.wirz@cloudrexx.com>
 * @package     cloudrexx
 * @subpackage  module_shop
 */
class ProductTest extends \Cx\Core\Test\Model\Entity\MySQLTestCase {

    protected $products = [
        1 => [
            'code' => 'code1',
            'category' => 1,
            'name'  => 'product1',
            'distribution' => \Cx\Modules\Shop\Controller\Distribution::TYPE_DELIVERY,
            'price' => 12.35,
            'active' => true,
            'ord'    => 0,
            'weight'    => 5,
            'pictures' => [
                1 => [
                    'file'  => 'cloudrexx.png',
                    'width' => '300',
                    'height'=> '300'
                ],
                2 => [
                    'file'  => 'mitgliedschaft.jpg',
                    'width' => '522',
                    'height'=> '538'
                ],
                3 => [
                    'file'  => '',
                    'width' => '',
                    'height'=> ''
                ],
            ],
        ],
        2 => [
            'code' => 'code2',
            'category' => 1,
            'name'  => 'product2',
            'distribution' => \Cx\Modules\Shop\Controller\Distribution::TYPE_DELIVERY,
            'price' => 23.45,
            'active' => true,
            'ord'    => 1,
            'weight'    => 10,
            'pictures' => [
                1 => [
                    'file'  => 'become_a_member.jpg',
                    'width' => '518',
                    'height'=> '423'
                ],
                2 => [
                    'file'  => '',
                    'width' => '',
                    'height'=> ''
                ],
                3 => [
                    'file'  => 'htc_one_x.jpg',
                    'width' => '520',
                    'height'=> '291'
                ],
            ],
        ],
        3 => [
            'code' => 'code3',
            'category' => 1,
            'name'  => 'product3',
            'distribution' => \Cx\Modules\Shop\Controller\Distribution::TYPE_DELIVERY,
            'price' => 45.65,
            'active' => true,
            'ord'    => 2,
            'weight'    => 20,
            'pictures' => [
                1 => [
                    'file'  => '',
                    'width' => '',
                    'height'=> ''
                ],
                2 => [
                    'file'  => '',
                    'width' => '',
                    'height'=> ''
                ],
                3 => [
                    'file'  => 'htc_one_x.jpg',
                    'width' => '520',
                    'height'=> '291'
                ],
            ],
        ],
        4 => [
            'code' => 'code4',
            'category' => 1,
            'name'  => 'product4',
            'distribution' => \Cx\Modules\Shop\Controller\Distribution::TYPE_DELIVERY,
            'price' => 67.80,
            'active' => true,
            'ord'    => 3,
            'weight'    => 12,
            'pictures' => [
                1 => [
                    'file'  => '',
                    'width' => '',
                    'height'=> ''
                ],
                2 => [
                    'file'  => '',
                    'width' => '',
                    'height'=> ''
                ],
                3 => [
                    'file'  => '',
                    'width' => '',
                    'height'=> ''
                ],
            ],
        ],
        5 => [
            'code' => 'code5',
            'category' => 1,
            'name'  => 'product5',
            'distribution' => \Cx\Modules\Shop\Controller\Distribution::TYPE_DELIVERY,
            'price' => 121.30,
            'active' => true,
            'ord'    => 4,
            'weight'    => 31,
            'pictures' => [
                1 => [
                    'file'  => '',
                    'width' => '',
                    'height'=> ''
                ],
                2 => [
                    'file'  => 'become_a_member.jpg',
                    'width' => '518',
                    'height'=> '423'
                ],
                3 => [
                    'file'  => '',
                    'width' => '',
                    'height'=> ''
                ],
            ],
        ],
    ];

    protected $categories = [
        1 => [
            'name'      => 'cat1',
            'short'     => 'short cat1',
            'long'      => 'long cat1',
            'parentId'  => 0,
            'active'    => true,
            'ord'       => 0,
            'picture'   => 'cloudrexx_logo.png',
        ],
    ];

    protected $noPicture = [
        'file' => \Cx\Modules\Shop\Controller\ShopLibrary::noPictureName,
        'thumbnail'=> \Cx\Modules\Shop\Controller\ShopLibrary::noPictureName,
        'thumbnails'=> [],
    ];

    public function setUp(): void {
        parent::setUp();

        // drop once CLX-1856 has been implemented
        if (!defined('FRONTEND_LANG_ID')) {
            define('FRONTEND_LANG_ID', 1);
        }

        \Cx\Modules\Shop\Controller\ShopCategories::deleteAll();
        foreach ($this->categories as &$category) {
            $category['ref'] = new \Cx\Modules\Shop\Controller\ShopCategory(
                $category['name'],
                $category['short'],
                $category['long'],
                $category['parentId'],
                $category['active'],
                $category['ord']
            );
            $category['ref']->picture($category['picture']);
            $category['ref']->insert();
            $category['id'] = $category['ref']->id();
        }
        $thumbnailFormats = static::$cx->getMediaSourceManager()->getThumbnailGenerator()->getThumbnails();
        foreach ($thumbnailFormats as $format) {
            $this->noPicture['thumbnails'][$format['name']] = preg_replace(
                '/^(.*)(\.[^.]+)$/',
                '\1'.$format['value'].'\2',
                $this->noPicture['file']
            );
        }
        foreach ($this->products as &$product) {
            $product['ref'] = new \Cx\Modules\Shop\Controller\Product(
                $product['code'],
                $this->categories[$product['category']]['id'],
                $product['name'],
                $product['distribution'],
                $product['price'],
                $product['active'],
                $product['ord'],
                $product['weight']
            );
            $pictures = $product['pictures'];
            array_walk(
                $pictures,
                function(&$v) {
                    $v = join(
                        '?',
                        array_map(
                            'base64_encode',
                            $v
                        )
                    );
                }
            );
            $product['thumbnails'] = array_map(
                function($v) use ($thumbnailFormats) {
                    $thumbs = [];
                    foreach ($thumbnailFormats as $format) {
                        $thumbs[$format['name']] = preg_replace(
                            '/^(.*)(\.[^.]+)$/',
                            '\1'.$format['value'].'\2',
                            $v['file']
                        );
                    }
                    return $thumbs;
                },
                $product['pictures']
            );
            \Cx\Core\Setting\Controller\Setting::init('Shop');
            $noPictureSize = getimagesize(
                static::$cx->getWebsiteImagesShopPath() . '/' . $this->noPicture['file']
            );
            \Cx\Modules\Shop\Controller\ShopLibrary::scaleImageSizeToThumbnail($noPictureSize);
            $this->noPicture['size'] = $noPictureSize[3];
            array_walk(
                $product['pictures'],
                function(&$v) {
                    if (empty($v['file'])) {
                        return;
                    }
                    $arrSize = [intval($v['width']), intval($v['height'])];
                    \Cx\Modules\Shop\Controller\ShopLibrary::scaleImageSizeToThumbnail($arrSize);
                    $v['size'] = $arrSize[3];
                }
            );
            $product['ref']->pictures(join(':', $pictures));
            $product['ref']->shown_on_startpage(1);
            $product['ref']->short('');
            $product['ref']->long('');
            $product['ref']->keywords('');
            $product['ref']->uri('');
            $product['ref']->store();
            $product['id']=$product['ref']->id();
        }
    }

    public function testParse_products_blocks_Fixed_Images() {
        \Cx\Core\Setting\Controller\Setting::init('Shop');
        \Cx\Core\Setting\Controller\Setting::set('image_parse_behaviour', \Cx\Modules\Shop\Controller\ShopLibrary::IMAGE_PARSE_BEHAVIOUR_FIXED);
        \Cx\Core\Setting\Controller\Setting::update('image_parse_behaviour');
        \Cx\Core\Setting\Controller\Setting::set('image_use_no_pictrue', false);
        \Cx\Core\Setting\Controller\Setting::update('image_use_no_pictrue');
        $this->assertEquals(
            $this->loadExpectedTemplateResult('ProductImageData_Fixed.html'),
            $this->getParsedImageData()
        );
    }

    public function testParse_products_blocks_Filling_Images() {
        \Cx\Core\Setting\Controller\Setting::init('Shop');
        \Cx\Core\Setting\Controller\Setting::set('image_parse_behaviour', \Cx\Modules\Shop\Controller\ShopLibrary::IMAGE_PARSE_BEHAVIOUR_FILLING);
        \Cx\Core\Setting\Controller\Setting::update('image_parse_behaviour');
        \Cx\Core\Setting\Controller\Setting::set('image_use_no_pictrue', false);
        \Cx\Core\Setting\Controller\Setting::update('image_use_no_pictrue');
        $this->assertEquals(
            $this->loadExpectedTemplateResult('ProductImageData_Filling.html'),
            $this->getParsedImageData()
        );
    }

    public function testParse_products_blocks_Fixed_Images_with_NoPicture() {
        \Cx\Core\Setting\Controller\Setting::init('Shop');
        \Cx\Core\Setting\Controller\Setting::set('image_parse_behaviour', \Cx\Modules\Shop\Controller\ShopLibrary::IMAGE_PARSE_BEHAVIOUR_FIXED);
        \Cx\Core\Setting\Controller\Setting::update('image_parse_behaviour');
        \Cx\Core\Setting\Controller\Setting::set('image_use_no_pictrue', true);
        \Cx\Core\Setting\Controller\Setting::update('image_use_no_pictrue');
        $this->assertEquals(
            $this->loadExpectedTemplateResult('ProductImageData_Fixed_NoPicture.html'),
            $this->getParsedImageData()
        );
    }

    public function testParse_products_blocks_Filling_Images_with_NoPicture() {
        \Cx\Core\Setting\Controller\Setting::init('Shop');
        \Cx\Core\Setting\Controller\Setting::set('image_parse_behaviour', \Cx\Modules\Shop\Controller\ShopLibrary::IMAGE_PARSE_BEHAVIOUR_FILLING);
        \Cx\Core\Setting\Controller\Setting::update('image_parse_behaviour');
        \Cx\Core\Setting\Controller\Setting::set('image_use_no_pictrue', true);
        \Cx\Core\Setting\Controller\Setting::update('image_use_no_pictrue');
        $this->assertEquals(
            $this->loadExpectedTemplateResult('ProductImageData_Filling_NoPicture.html'),
            $this->getParsedImageData()
        );
    }

    public function getParsedMetaImage($productId) {
        global $_ARRAYLANG;
        $_ARRAYLANG = \Env::get('init')->getComponentSpecificLanguageData('Shop');
        $original_REQUEST = &$_REQUEST;
        $_REQUEST = ['productId' => $this->products[$productId]['id']];
        \Cx\Modules\Shop\Controller\Shop::view_product_overview();
        $_REQUEST = &$original_REQUEST;
        return \Cx\Modules\Shop\Controller\Shop::getPageMetaImage();
    }

    public function testParseMetaImageOfProduct1() {
        $productId = 1;
        $this->assertEquals(
            $this->loadExpectedTemplateResult('ProductImageData_Metadata.html', $productId, 1),
            $this->getParsedMetaImage($productId)
        );
    }

    public function testParseMetaImageOfProduct2() {
        $productId = 2;
        $this->assertEquals(
            $this->loadExpectedTemplateResult('ProductImageData_Metadata.html', $productId, 1),
            $this->getParsedMetaImage($productId)
        );
    }

    public function testParseMetaImageOfProduct3() {
        $productId = 3;
        $this->assertEquals(
            $this->loadExpectedTemplateResult('ProductImageData_Metadata.html', $productId, 3),
            $this->getParsedMetaImage($productId)
        );
    }

    public function testParseMetaImageOfProduct4() {
        $productId = 4;
        $this->assertEquals(
            $this->loadExpectedTemplateResult('CategoryImageData_Metadata.html', $this->products[$productId]['category']),
            $this->getParsedMetaImage($productId)
        );
    }

    public function testParseMetaImageOfProduct5() {
        $productId = 5;
        $this->assertEquals(
            $this->loadExpectedTemplateResult('ProductImageData_Metadata.html', $productId, 2),
            $this->getParsedMetaImage($productId)
        );
    }

    protected function getParsedImageData() {
        global $_ARRAYLANG;
        $_ARRAYLANG = \Env::get('init')->getComponentSpecificLanguageData('Shop');
        $input = file_get_contents($this->getComponent('Shop')->getDirectory() . '/Testing/UnitTest/Data/ProductImageData.tpl');
        $template = new \Cx\Core\Html\Sigma();
        $template->setTemplate($input);
        \Cx\Modules\Shop\Controller\Shop::parse_products_blocks($template);
        return trim($template->get());
    }

    protected function loadExpectedTemplateResult($resultFile, $idx = 0, $img = 0) {
        $imagePath = static::$cx->getWebsiteImagesShopWebPath() . '/';
        $hostUrl = \Cx\Core\Routing\Url::fromDocumentRoot();
        $hostUrl->isFrontend(false);
        $hostUrl = rtrim($hostUrl->toString(), '/');
        $data = file_get_contents(
            $this->getComponent('Shop')->getDirectory() . '/Testing/UnitTest/Data/' . $resultFile
        );
        $data = trim($data);
        return preg_replace_callback(
            '/{([^}]+)}/',
            function($match) use (&$idx, &$img, $imagePath, $hostUrl) {
                eval('$matchedStr = '.$match[1].';');
                return $matchedStr;
            },
            $data
        );
    }
}
