<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

namespace Cx\Core\User\Testing\UnitTest;

/**
 * Test User
 * @copyright   CLOUDREXX CMS - CLOUDREXX AG
 * @author      Reto Kohli <reto.kohli@comvation.com>
 * @version     1.0.0
 * @package     cloudrexx
 * @subpackage  lib_framework
 * @todo        More tests
 */
class UserTest extends \Cx\Core\Test\Model\Entity\ContrexxTestCase
{
    public function testMakePassword()
    {
        //\DBG::activate(DBG_PHP | DBG_LOG_FILE);
        // Times with logging to file enabled:
        //$loops = 0; // 160..165ms (system overhead)
        $loops = 1000; // 240..260ms (<0.1ms avg)
        //$loops = 10000; // ~950 ms (<0.08ms avg)
        while ($loops--) {
            $password = \User::make_password(
                random_int(5, 13), (bool) random_int(0, 1)
            );
            $this->assertNotEmpty($password);
            //\DBG::log('Password: ' . $password);
        }
        $this->assertEquals(6, strlen(\User::make_password(5)));
        $this->assertEquals(64, strlen(\User::make_password(65)));
    }

}
