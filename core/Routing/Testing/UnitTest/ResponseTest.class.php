<?php
/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * ResponseTest
 *
 * @copyright   CLOUDREXX CMS - CLOUDREXX AG
 * @author      Thomas Wirz <thomas.wirz@cloudrexx.com>
 * @package     cloudrexx
 * @subpackage  core_resolver
 */
namespace Cx\Core\Routing\Testing\UnitTest;

/**
 * ResponseTest
 *
 * @copyright   CLOUDREXX CMS - CLOUDREXX AG
 * @author      Thomas Wirz <thomas.wirz@cloudrexx.com>
 * @package     cloudrexx
 * @subpackage  core_resolver
 */
class ResponseTest extends \Cx\Core\Test\Model\Entity\ContrexxTestCase {
    public function testContentSecurityPolicySetHeaderException() {
        $this->expectException(
            \Cx\Lib\Net\Model\Entity\ResponseException::class
        );
        $response = new \Cx\Core\Routing\Model\Entity\Response(null);
        $response->setHeader(
            'Content-Security-Policy',
            'default-src \'self\''
        );
    }

    public function testContentSecurityPolicyHeader() {
        $cspHeader = 'Content-Security-Policy';
        $response = new \Cx\Core\Routing\Model\Entity\Response(null);

        // verify unset
        $this->assertEquals(
            false,
            isset($response->getHeaders()[$cspHeader])
        );

        // test initial setting
        $response->setContentSecurityPolicyDirective(
            'default-src',
            array('self')
        );
        $this->assertEquals(
            'default-src \'self\'',
            $response->getHeaders()[$cspHeader]
        );

        // test overwrite
        $response->setContentSecurityPolicyDirective(
            'default-src',
            array('none')
        );
        $this->assertEquals(
            'default-src \'none\'',
            $response->getHeaders()[$cspHeader]
        );

        // test multiple directives
        $response->setContentSecurityPolicyDirective(
            'frame-ancestors',
            array(
                'none',
                'https:'
            )
        );
        $this->assertEquals(
            'default-src \'none\'; frame-ancestors \'none\' https:',
            $response->getHeaders()[$cspHeader]
        );

        // test unsetting
        $response->setContentSecurityPolicyDirective(
            'default-src',
            array()
        );
        $this->assertEquals(
            'frame-ancestors \'none\' https:',
            $response->getHeaders()[$cspHeader]
        );

        // test complete unset
        $response->setContentSecurityPolicyDirective(
            'frame-ancestors',
            array()
        );
        $this->assertEquals(
            false,
            isset($response->getHeaders()[$cspHeader])
        );
    }
}

