<?php
declare(strict_types=1);

/**
 * Cloudrexx
 *
 * @link      https://www.cloudrexx.com
 * @copyright Cloudrexx AG
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

namespace Cx\Core\Routing\Testing\UnitTest;

/**
 * @copyright   Cloudrexx AG
 * @author      Thomas Wirz <thomas.wirz@cloudrexx.com>
 * @package     cloudrexx
 * @subpackage  core_routing
 */
class RequestTest extends \Cx\Core\Test\Model\Entity\ContrexxTestCase {
    protected static $server = [];

    public function setUp(): void {
        static::$server = $_SERVER;
    }

    public function tearDown(): void {
        $_SERVER = static::$server;
    }

    public function testGetClientIpWithoutProxy() {
        $cx = \Cx\Core\Core\Controller\Cx::instanciate();
        $config = $cx->getConfig()->getFullBaseConfig();
        $config['trustedReverseProxyCount'] = 0;
        $clientIp = '80.80.80.80';
        $_SERVER['REMOTE_ADDR'] = $clientIp;
        $this->assertEquals(
            $clientIp,
            \Cx\Core\Routing\Model\Entity\Request::getClientIp()
        );
    }

    public function testGetClientIpOverOneProxyWithOneTrusted() {
        $cx = \Cx\Core\Core\Controller\Cx::instanciate();
        $config = $cx->getConfig()->getFullBaseConfig();
        $config['trustedReverseProxyCount'] = 1;
        $proxyIp1 = '40.40.40.40';
        $clientIp = '80.80.80.80';
        $_SERVER['REMOTE_ADDR'] = $proxyIp1;
        $_SERVER['HTTP_X_FORWARDED_FOR'] = $clientIp;
        $this->assertEquals(
            $clientIp,
            \Cx\Core\Routing\Model\Entity\Request::getClientIp()
        );
    }

    public function testGetClientIpOverOneProxyWithoutOneTrusted() {
        $cx = \Cx\Core\Core\Controller\Cx::instanciate();
        $config = $cx->getConfig()->getFullBaseConfig();
        $config['trustedReverseProxyCount'] = 0;
        $proxyIp1 = '40.40.40.40';
        $clientIp = '80.80.80.80';
        $_SERVER['REMOTE_ADDR'] = $proxyIp1;
        $_SERVER['HTTP_X_FORWARDED_FOR'] = $clientIp;
        $this->assertEquals(
            $proxyIp1,
            \Cx\Core\Routing\Model\Entity\Request::getClientIp()
        );
    }

    public function testGetClientIpOverOneNonTransparentProxyWithOneTrusted() {
        $cx = \Cx\Core\Core\Controller\Cx::instanciate();
        $config = $cx->getConfig()->getFullBaseConfig();
        $config['trustedReverseProxyCount'] = 1;
        $proxyIp1 = '40.40.40.40';
        $clientIp = '80.80.80.80';
        $_SERVER['REMOTE_ADDR'] = $proxyIp1;
        $_SERVER['HTTP_X_FORWARDED_FOR'] = $clientIp . ', ' . $proxyIp1;
        $this->assertEquals(
            $clientIp,
            \Cx\Core\Routing\Model\Entity\Request::getClientIp()
        );
    }

    public function testGetClientIpOverOneNonTransparentProxyWithoutOneTrusted() {
        $cx = \Cx\Core\Core\Controller\Cx::instanciate();
        $config = $cx->getConfig()->getFullBaseConfig();
        $config['trustedReverseProxyCount'] = 0;
        $proxyIp1 = '40.40.40.40';
        $clientIp = '80.80.80.80';
        $_SERVER['REMOTE_ADDR'] = $proxyIp1;
        $_SERVER['HTTP_X_FORWARDED_FOR'] = $clientIp . ', ' . $proxyIp1;
        $this->assertEquals(
            $proxyIp1,
            \Cx\Core\Routing\Model\Entity\Request::getClientIp()
        );
    }

    public function testGetClientIpOverTwoProxiesWithoutOneTrusted() {
        $cx = \Cx\Core\Core\Controller\Cx::instanciate();
        $config = $cx->getConfig()->getFullBaseConfig();
        $config['trustedReverseProxyCount'] = 0;
        $proxyIp1 = '20.20.20.20';
        $proxyIp2 = '40.40.40.40';
        $clientIp = '80.80.80.80';
        $_SERVER['REMOTE_ADDR'] = $proxyIp2;
        $_SERVER['HTTP_X_FORWARDED_FOR'] = $clientIp . ', ' . $proxyIp1 . ', ' . $proxyIp2;
        $this->assertEquals(
            $proxyIp2,
            \Cx\Core\Routing\Model\Entity\Request::getClientIp()
        );
    }

    public function testGetClientIpOverTwoProxiesWithOneTrusted() {
        $cx = \Cx\Core\Core\Controller\Cx::instanciate();
        $config = $cx->getConfig()->getFullBaseConfig();
        $config['trustedReverseProxyCount'] = 1;
        $proxyIp1 = '20.20.20.20';
        $proxyIp2 = '40.40.40.40';
        $clientIp = '80.80.80.80';
        $_SERVER['REMOTE_ADDR'] = $proxyIp2;
        $_SERVER['HTTP_X_FORWARDED_FOR'] = $clientIp . ', ' . $proxyIp1 . ', ' . $proxyIp2;
        $this->assertEquals(
            $proxyIp1,
            \Cx\Core\Routing\Model\Entity\Request::getClientIp()
        );
    }

    public function testGetClientIpOverTwoProxiesWithTwoTrusted() {
        $cx = \Cx\Core\Core\Controller\Cx::instanciate();
        $config = $cx->getConfig()->getFullBaseConfig();
        $config['trustedReverseProxyCount'] = 2;
        $proxyIp1 = '20.20.20.20';
        $proxyIp2 = '40.40.40.40';
        $clientIp = '80.80.80.80';
        $_SERVER['REMOTE_ADDR'] = $proxyIp2;
        $_SERVER['HTTP_X_FORWARDED_FOR'] = $clientIp . ', ' . $proxyIp1 . ', ' . $proxyIp2;
        $this->assertEquals(
            $clientIp,
            \Cx\Core\Routing\Model\Entity\Request::getClientIp()
        );
    }

    public function testGetClientIpOverThreeProxiesWithTwoTrusted() {
        $cx = \Cx\Core\Core\Controller\Cx::instanciate();
        $config = $cx->getConfig()->getFullBaseConfig();
        $config['trustedReverseProxyCount'] = 2;
        $proxyIp1 = '20.20.20.20';
        $proxyIp2 = '40.40.40.40';
        $proxyIp3 = '60.60.60.60';
        $clientIp = '80.80.80.80';
        $_SERVER['REMOTE_ADDR'] = $proxyIp3;
        $_SERVER['HTTP_X_FORWARDED_FOR'] = $clientIp . ', ' . $proxyIp1 . ', ' . $proxyIp2 . ', ' . $proxyIp3;
        $this->assertEquals(
            $proxyIp1,
            \Cx\Core\Routing\Model\Entity\Request::getClientIp()
        );
    }

    public function testGetClientIpOverThreeProxiesWithThreeTrusted() {
        $cx = \Cx\Core\Core\Controller\Cx::instanciate();
        $config = $cx->getConfig()->getFullBaseConfig();
        $config['trustedReverseProxyCount'] = 3;
        $proxyIp1 = '20.20.20.20';
        $proxyIp2 = '40.40.40.40';
        $proxyIp3 = '60.60.60.60';
        $clientIp = '80.80.80.80';
        $_SERVER['REMOTE_ADDR'] = $proxyIp3;
        $_SERVER['HTTP_X_FORWARDED_FOR'] = $clientIp . ', ' . $proxyIp1 . ', ' . $proxyIp2 . ', ' . $proxyIp3;
        $this->assertEquals(
            $clientIp,
            \Cx\Core\Routing\Model\Entity\Request::getClientIp()
        );
    }

    public function testGetClientIpOverThreeProxiesWithOneTrustedButIgnoreTrust() {
        $cx = \Cx\Core\Core\Controller\Cx::instanciate();
        $config = $cx->getConfig()->getFullBaseConfig();
        $config['trustedReverseProxyCount'] = 1;
        $proxyIp1 = '20.20.20.20';
        $proxyIp2 = '40.40.40.40';
        $proxyIp3 = '60.60.60.60';
        $clientIp = '80.80.80.80';
        $_SERVER['REMOTE_ADDR'] = $proxyIp3;
        $_SERVER['HTTP_X_FORWARDED_FOR'] = $clientIp . ', ' . $proxyIp1 . ', ' . $proxyIp2 . ', ' . $proxyIp3;
        $this->assertEquals(
            $clientIp,
            \Cx\Core\Routing\Model\Entity\Request::getClientIp(false)
        );
    }
}
