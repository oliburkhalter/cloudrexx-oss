# Set the path, relative from the document root, to Contrexx.
# Technical note: This is ASCMS_PATH_OFFSET
# I.e.: /
RewriteBase   %PATH_ROOT_OFFSET%

# Deny direct access to directories containing sensitive data
RewriteCond %{ENV:REDIRECT_END} !1
RewriteCond %{REQUEST_URI} ^/(config|tmp|websites|core/.*/Data|core_modules/.*/Data|modules/.*/Data)/
RewriteRule . - [F]

# Resolve language specific sitemap.xml
RewriteRule ^([a-z]{1,2}(?:-[A-Za-z]{2,4})?)\/sitemap.xml$ sitemap_$1.xml [L,NC]

# Allow directory index files
RewriteCond %{REQUEST_FILENAME}/index.php -f
RewriteRule   .  %{REQUEST_URI}/index.php [L,QSA]

# Redirect all requests to non-existing files to Contrexx
RewriteCond   %{REQUEST_FILENAME}  !-f
RewriteRule   .  index.php?__cap=%{REQUEST_URI} [L,QSA]

# Add captured request to index files
RewriteRule ^index.php index.php?__cap=%{REQUEST_URI} [L,QSA]
