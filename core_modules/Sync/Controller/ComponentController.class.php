<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 * 
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */
 
/**
 * Main controller for Sync
 * 
 * @copyright   Cloudrexx AG
 * @author Michael Ritter <michael.ritter@cloudrexx.com>
 * @package cloudrexx
 * @subpackage core_modules_sync
 */

namespace Cx\Core_Modules\Sync\Controller;

/**
 * Main controller for Sync
 * 
 * @copyright   Cloudrexx AG
 * @author Michael Ritter <michael.ritter@cloudrexx.com>
 * @package cloudrexx
 * @subpackage core_modules_sync
 * @todo In order to respect permission layer, this should not make direct use of doctrine (except for handling \Cx\Core_Modules\Sync\Model\Entity\... entities)
 */
class ComponentController extends \Cx\Core\Core\Model\Entity\SystemComponentController implements \Cx\Core\Event\Model\Entity\EventListener {
    /**
     * @var array List of supported API versions
     */
    protected $supportedApiVersions = array('v1');
    
    /**
     * @var array Two dimensional array array(<entityClassName> => <listOfSyncEntities)
     */
    protected $syncs = array();
    
    /**
     * @var boolean Allows suppress pushing to remotes
     */
    protected $doPush = true;
    
    protected $alreadySyncedEntities = array();
    
    protected $addIds = array();
    
    protected $spooledPushes = array();
    
    protected $unspooling = false;

    /**
     * @var \Cx\Core_Modules\Sync\Model\Entity\Spooler Holds changeset to be
     * processed (syncs to push)
     */
    protected $spooler = null;

    /**
     * @var array   Holds the primary key of the entity being persisted (on
     * the remote side)
     */
    protected $currentInsertId = array();

    /**
     * On remote side, this is set to the changeset's foreign host
     */
    protected $foreignHost = '';

    const SYNC_COMMAND_NAME= 'sync';
    
    /**
     * Returns all Controller class names for this component (except this)
     * 
     * Be sure to return all your controller classes if you add your own
     * @return array List of Controller class names (without namespace)
     */
    public function getControllerClasses() {
        return array();
    }
    
    /**
     * Registers all needed events for all syncronized entities
     * 
     * This includes listeners for local side (all entities that are to be synced)
     * and listeners for remote side (all changes of ID that are in mapping table)
     */
    public function registerEvents() {
        $doctrineEvents = array(
            \Doctrine\ORM\Events::preRemove,
            \Doctrine\ORM\Events::postRemove,
            \Doctrine\ORM\Events::postPersist,
            \Doctrine\ORM\Events::postUpdate,
            \Doctrine\ORM\Events::postFlush,
        );
        
        $this->syncs = array();
        $em = $this->cx->getDb()->getEntityManager();
        $syncRepo = $em->getRepository($this->getNamespace() . '\Model\Entity\Sync');
        $syncs = $syncRepo->findAll();
        foreach ($syncs as $sync) {
            if (!$sync->getActive()) {
                continue;
            }
            // get entity class name
            $entityClassName = $sync->getDataAccess()->getDataSource()->getIdentifier();
            if (!isset($this->syncs[$entityClassName])) {
                $this->syncs[$entityClassName] = array();
            }
            $this->syncs[$entityClassName][] = $sync;
            if ($this->useHackForCalendar($entityClassName)) {
                $sync->setOldHostEntitiesIncludingLegacy($sync->getHostEntitiesIncludingLegacy(false));
            }
        }

        // event listener must be registered independently of any syncs
        // ({@see $this->syncs}) as on the remote side it is most likely that
        // no syncs have been set up, but we still have to listen to model
        // events as otherwise we would not be able to track foreign sync
        // relations.
        foreach ($doctrineEvents as $doctrineEvent) {
            $this->cx->getEvents()->addEventListener(
                'model/' . $doctrineEvent,
                $this
            );
        }

        if (!$this->syncs) {
            return;
        }

        register_shutdown_function(array($this, 'unspool'));
    }
    
    /**
     * Returns a list of command mode commands provided by this component
     * @return array List of command names
     */
    public function getCommandsForCommandMode() {
        return array(
            static::SYNC_COMMAND_NAME => new \Cx\Core_Modules\Access\Model\Entity\Permission(
                array('http', 'https'), // allowed protocols
                array(
                    'get',
                    'post',
                    'put',
                    'delete',
                    'trace',
                    'options',
                    'head',
                    'cli',
                ),   // allowed methods
                false                   // requires login
            ),
        );
    }

    /**
     * Returns the description for a command provided by this component
     * @param string $command The name of the command to fetch the description from
     * @param boolean $short Wheter to return short or long description
     * @return string Command description
     */
    public function getCommandDescription($command, $short = false) {
        switch ($command) {
            case static::SYNC_COMMAND_NAME:
                if ($short) {
                    return 'Allows synchronizing data objects using the RESTful API';
                }
                return 'Allows synchronizing data objects using the RESTful API' . "\n\n" .
                    'FETCHING' . "\n" .
                    "\t" . 'Usage: ' . static::SYNC_COMMAND_NAME . ' <apiVersion> <outputModule> <dataSource> (<elementId>) (apikey=<apiKey>) (<options>)' . "\n\n" .
                    'PUSHING' . "\n" .
                    "\t" . 'Usage: ' . static::SYNC_COMMAND_NAME . ' push';
            default:
                return '';
        }
    }
    
    /**
     * Execute one of the commands listed in getCommandsForCommandMode()
     *
     * <domain>(/<offset>)/api/sync/<apiVersion>/<outputModule>/<dataSource>/<parameters>[(?apikey=<apikey>(&<options>))|?<options>]
     * @see getCommandsForCommandMode()
     * @param string $command Name of command to execute
     * @param array $arguments List of arguments for the command
     * @return void
     */
    public function executeCommand($command, $arguments, $dataArguments = array()) {
        try {
            switch ($command) {
                case static::SYNC_COMMAND_NAME:
                    $verboseArgument = '-v';
                    $verbose = false;
                    if (in_array($verboseArgument, $arguments)) {
                        $verbose = true;
                        unset($arguments[array_search($verboseArgument, $arguments)]);
                    }
                    // do not calculate new changes made during this request
                    $this->doPush = false;
                    
                    // local side code
                    if (count($arguments) == 1 && $arguments[0] == 'push') {
                        if (!$this->cx->isCliCall()) {
                            return;
                        }
                        $this->pushChanges($verbose);
                        break;
                    }
                    
                    // remote side code
                    $this->sync($command, $arguments, $dataArguments);
                    break;
            }
        } catch (\Exception $e) {
            // This should only be used if API cannot handle the request at all.
            // Most exceptions should be catched inside the API!
            http_response_code(400); // BAD REQUEST
            echo 'Exception of type "' . get_class($e) . '" with message "' . $e->getMessage() . '"';
            \DBG::log($e->getTraceAsString());
        }
    }

    /**
     * Call to process the changesets hold in the spooler.
     * This will initiate the sync command to push the model changes
     * to the remotes.
     */
    public function unspool() {
        // persist spooler
        if ($this->unspooling || !isset($this->spooler)) {
            return;
        }
        $this->unspooling = true;
        $em = $this->cx->getDb()->getEntityManager();
        foreach ($this->spooler->getSpool() as $i=>$change) {
            $change->setHosts($change->getOriginSync()->getRelatedHosts($change->getOriginEntityIndexData()));
            if (!count($change->getHosts())) {
                continue;
            }
            $em->persist($change);
        }
        if (!$em->getUnitOfWork()->hasPendingInsertions()) {
            // no changes
            return;
        }

        // flush changes to db
        $em->flush();

        // execute `./cx sync push` asynchronously
        $code = $this->getComponent('Core')->execAsync(
            static::SYNC_COMMAND_NAME,
            array(
                'push',
            )
        );
        if ($code !== 0) {
            \DBG::msg('SYNC ERROR: push failed');
        }

        $this->spooler = new \Cx\Core_Modules\Sync\Model\Entity\Spooler();
        $this->unspooling = false;
    }
    
    /**
     * This is the handler for remote side: handles incoming changes
     * @param string $command Always "sync"
     * @param array $arguments Arguments for API, first entry is API version
     * @param array  $dataArguments (optional) List of data arguments for the command
     * @todo pretending delete was successful does not work for other output methods than json
     * @todo update ID fields does not work as expected
     */
    public function sync($command, $arguments, $dataArguments) {
        $method = $this->cx->getRequest()->getHttpRequestMethod();
        $apiVersion = array_shift($arguments); // shift api version
        
        if (!in_array($apiVersion, $this->supportedApiVersions)) {
            throw new \BadMethodCallException('Unsupported API version: "' . $apiVersion . '"');
        }
        if (empty($arguments[0])) {
            throw new \InvalidArgumentException('Not enough arguments');
        }
        $outputModule = $this->getComponent('DataAccess')->getOutputModule($arguments[0]);
        $response = new \Cx\Core_Modules\DataAccess\Model\Entity\ApiResponse();
        if (empty($arguments[1])) {
            throw new \InvalidArgumentException('Not enough arguments');
        }

        $dataSource = $this->getComponent('DataAccess')->getDataSource(
            $arguments[1]
        );
        $elementId = array();
        $this->cx->getEvents()->addEventListener('preDistantEntityLoad', $this);

        $argumentKeys = array_keys($arguments);
        if (!isset($arguments[2])) {
            // API would produce a 404 here
            throw new \BadMethodCallException('No element given');
        }

        $primaryKeyNames = $dataSource->getIdentifierFieldNames();
        if (!$primaryKeyNames || !is_array($primaryKeyNames)) {
            throw new \Cx\Core\Core\Controller\InstanceException(
                'Missing identifier'
            );
        }

        // Positional keys are integers; skip the first two (see above)
        $positionalArgumentKeys = array_filter(
            array_keys($arguments),
            function($key) {
                return is_int($key) && $key > 1;
            }
        );
        $keyName = null;
        foreach ($positionalArgumentKeys as $index) {
            if ($primaryKeyNames) {
                $keyName = array_shift($primaryKeyNames);
                $elementId[$keyName] = '';
            }
            // Presume surplus values to be path components, and append
            // them to the last primary key value, separated by slashes.
            // $keyName is supposedly "filename" in this case.
            if (!empty($elementId[$keyName])) {
                $elementId[$keyName] .= '/';
            }
            $elementId[$keyName] .= $arguments[$index];
            if (!isset($dataArguments[$keyName])) {
                $dataArguments[$keyName] = $arguments[$index];
            }
        }
        $this->currentInsertId = $elementId;
    
        $referrerParts = parse_url($_SERVER['HTTP_REFERRER']);
        $this->foreignHost = $referrerParts['host'];
        
        $em = $this->cx->getDb()->getEntityManager();
        $entityType = $dataSource->getIdentifier();
        $mapping = $this->getIdMapping($entityType, $this->foreignHost, $elementId);

        $passthrough = false;
        $entityRepository = $em->getRepository($entityType);
        switch ($method) {
            case 'delete':
                if ($mapping) {
                    $entity = $entityRepository->findOneBy($mapping->getLocalId());
                } else {
                    $entity = $entityRepository->findOneBy($elementId);
                    $passthrough = true;
                }
                if (!$entity) {
                    // pretend everything is ok
                    $response->setStatus(
                        \Cx\Core_Modules\DataAccess\Model\Entity\ApiResponse::STATUS_OK
                    );
                    $response->setData(array());
                    $response->send($outputModule);
                    return;
                }
                break;
            case 'post':
                // if a new entity should be created that we already have,
                // this POST request has been made twice and can be dropped.
                // We simply pretend everything went fine:
                $response->setStatus(
                    \Cx\Core_Modules\DataAccess\Model\Entity\ApiResponse::STATUS_OK
                );
                if ($mapping) {
                    $entity = $entityRepository->findOneBy($mapping->getLocalId());
                } else {
                    // This case handles incoming updates from replica to primary.
                    // In this case no ID mapping needs to be done as this is
                    // already done on replica.
                    // This leads to problems with normal (non-bidirectional)
                    // sync if an ID on primary exists locally on replica.
                    //
                    // Therefore this is disabled as a fix for one-way sync.
                    // Disabling this breaks the current implementation for
                    // bi-directional sync.
                    // See CLX-4239 for what needs to be done to make bi-
                    // directional sync work again.
                    //$entity = $entityRepository->findOneBy($elementId);
                }
                if ($mapping && $entity) {
                    $response->setData($this->getEntityIndexData($entity));
                    $response->send($outputModule);
                    return;
                }
                if ($entity) {
                    $passthrough = true;
                    $this->cx->getRequest()->setHttpRequestMethod('put');
                    break;
                }
                if ($mapping) {
                    // we have a mapping for a non-existing entity. This shouldn't
                    // happen. Let's self-heal:
                    $em->remove($mapping);
                    $em->flush();
                }

            case 'put':
                if ($mapping) {
                    $entity = $entityRepository->findOneBy($mapping->getLocalId());
                    if ($entity) {
                        break;
                    }
                    $em->remove($mapping);
                    $em->flush();
                }
                // pretend it's a new element
                // in order to do so we need to:
                // - tell api it's a POST request
                // - make sure our eventlistener thinks it's a POST request
                $this->cx->getRequest()->setHttpRequestMethod('post');
                break;
        }

        $primaryKeyNames = $dataSource->getIdentifierFieldNames();
        if (!$passthrough) {
            $this->replacePrimaryKey(
                $this->foreignHost,
                $entityType,
                $primaryKeyNames,
                $dataArguments,
                $elementId
            );
        }
        $i = 2;
        foreach ($primaryKeyNames as $fieldName) {
            $arguments[$i] = $dataArguments[$fieldName] ?? '';
            if (isset($dataArguments[$fieldName])) {
                // unset primary key values in data, except for langId of calendar entities
                if (!$this->useHackForCalendar($entityType) && $fieldName != 'langId') {
                    unset($dataArguments[$fieldName]);
                }
            }
            $i++;
        }

        // Route to API
        $this->getComponent('DataAccess')->executeCommand(
            $apiVersion,
            $arguments,
            $dataArguments
        );
    }

    /**
     * Pushes changes to remote (local side) and updates mapping table (remote side)
     * @param string $eventName Name of the triggered event
     * @param object $eventArgs Doctrine event args
     * @todo update ID fields does not work as expected
     */
    public function onEvent($eventName, array $eventArgs) {
        $em = $this->cx->getDb()->getEntityManager();

        if ($eventName == 'preDistantEntityLoad') {
            $primaryKeyIdentifier = &$eventArgs['targetId'];
            $this->replacePrimaryKey(
                $this->foreignHost,
                $eventArgs['targetEntityClassName'],
                array_keys($primaryKeyIdentifier),
                $primaryKeyIdentifier,
                $eventArgs['targetId']
            );
            return;
        }

        if (substr($eventName, 6) != \Doctrine\ORM\Events::postFlush) {
            try {
                $dlea = current($eventArgs);
                $entity = $dlea->getEntity();
                $entityClassName = get_class($entity);
                $entityIndexData = $this->getEntityIndexData($entity);
            } catch (\Exception $e) {
                return; // temporary workaround for YAML entities
            }
            
            // We don't want to loop!
            if (
                substr($entityClassName, 0, strlen($this->getNamespace())) == $this->getNamespace() ||
                $entityClassName == 'Cx\Core_Modules\SysLog\Model\Entity\Log'
            ) {
                return;
            }
        }
        
        switch (substr($eventName, 6)) {
            // entity was dropped
            case \Doctrine\ORM\Events::preRemove:
                // remote side code
                $this->handleChange('delete', $entityClassName, $entityIndexData, $entity);
                break;
            // entity was added
            case \Doctrine\ORM\Events::postPersist:
                // remote side code
                if (!$this->doPush) {
                    // note: this code is executed after a new entity has been
                    // added through a DataAccess endpoint.
                    // Let's remember mapping information so that sync will be
                    // able to locate that entity again for post updates.
                    if (!isset($this->addIds[$entityClassName])) {
                        $this->addIds[$entityClassName] = array();
                    }
                    $mapping = new \Cx\Core_Modules\Sync\Model\Entity\IdMapping();
                    $referrerParts = parse_url($_SERVER['HTTP_REFERRER']);
                    $mapping->setForeignHost($referrerParts['host']);
                    $mapping->setEntityType($entityClassName);
                    $mapping->setForeignId($this->currentInsertId);
                    $mapping->setLocalId($entityIndexData);
                    $this->addIds[$entityClassName][] = $mapping;
                    break;
                }
                
                // local side code
                $this->handleChange('post', $entityClassName, $entityIndexData, $entity);
                break;
            // entity was updated
            case \Doctrine\ORM\Events::postUpdate:
                // remote side code
                // @todo: set old/new ID
                /*$oldId = ??;
                $newId = ??;
                $mappings = $mappingRepo->findBy(array(
                    'entityType' => $entityClassName,
                    'localId' => $oldId,
                ));
                foreach ($mappings as $mapping) {
                    $em->setLocalId($newId);
                }
                $em->flush();*/
                
                // local side code
                $this->handleChange('put', $entityClassName, $entityIndexData, $entity);
                break;
            case \Doctrine\ORM\Events::postFlush:
                // remote side code
                $doFlush = count($this->addIds);
                foreach ($this->addIds as $entityClassName=>$addIds) {
                    foreach ($addIds as $i=>$mapping) {
                        $em->persist($mapping);
                        unset($this->addIds[$entityClassName][$i]);
                    }
                    unset($this->addIds[$entityClassName]);
                }
                if ($doFlush) {
                    $em->flush();
                }
                break;
            default:
                return;
        }
    }
    
    /**
     * Spools changes to be pushed to remote on local side
     * @param string $eventType One of "post", "put", "delete"
     * @param string $entityClassName Classname of the entity to update
     * @param array $entityIndexData Field=>value-type array with primary key data
     * @param \Cx\Model\Base\EntityBase $entity Changed entity
     * @todo: Push relations first
     * @todo: This does not spool yet, instead it writes changes instantly
     */
    public function handleChange($eventType, $entityClassName, $entityIndexData, $entity) {
        if (!isset($this->spooler)) {
            $this->spooler = new \Cx\Core_Modules\Sync\Model\Entity\Spooler();
        }
        
        // suppress push on incoming changes (allows two-way sync)
        if (!$this->doPush) {
            return;
        }
        
        // do not sync entities that are not configured to be synced
        if (!isset($this->syncs[$entityClassName])) {
            return;
        }
        
        // calculate relations
        $entityClassName = get_class($entity);
        if ($entityClassName == 'Doctrine\ORM\PersistentCollection') {
            $entityClassName = $entity->getTypeClass()->name;
        }
        foreach ($this->syncs[$entityClassName] as $sync) {
            $sync->calculateRelations($this->spooler, $eventType, $entityClassName, $entityIndexData, $entity);
        }
    }

    /**
     * Push any pending changes to the set replicas.
     *
     * @param   boolean $verbose    Set to TRUE for continuous log output. If
     *                              not set, the log output will be buffered
     *                              till the end.
     *
     */
    protected function pushChanges($verbose = false) {
        $em = $this->cx->getDb()->getEntityManager();
        if (!$verbose) {
            ob_start();
        }
        
        $i = 0;
        $severity = 'INFO';
        // for as long as there are changes of non-locked hosts
        $fiveMinutesAgo = $this->getComponent(
            'DateTime'
        )->createDateTimeForDb('5 minutes ago');
        $query = $em->createQuery('
            SELECT
                h
            FROM
                ' . $this->getNamespace() . '\Model\Entity\Host h
            WHERE
                h.state = 0 OR
                (
                    h.state = 1 AND
                    h.lastUpdate < :fiveMinutesAgo
                )
        ')->setParameter(
            'fiveMinutesAgo',
            $fiveMinutesAgo->format('Y-m-d H:i:s')
        );
        $query->useResultCache(false);
        $nonLockedHosts = $query->getResult();
        $hasNonLockedChanges = false;
        foreach ($nonLockedHosts as $host) {
            if (count($host->getChanges())) {
                $hasNonLockedChanges = true;
                break;
            }
        }
        while ($hasNonLockedChanges) {
            // foreach host
            $log = 'Processing changes to ' . count($nonLockedHosts) . ' host(s)';
            echo $log . "\n";
            \DBG::msg($log);
            foreach ($nonLockedHosts as $host) {
                // skip if locked
                $em->refresh($host);
                if (!$host->lock()) {
                    $log = 'Host ' . $host->getHost() . ' locked, skipping';
                    echo $log . "\n";
                    \DBG::msg($log);
                    continue;
                }
                // sync all changes
                $log = 'Processing ' . count($host->getChanges()) . ' change(s)';
                echo $log . "\n";
                \DBG::msg($log);
                foreach ($host->getChanges() as $change) {
                    if (!$host->handleChange($change)) {
                        $severity = 'WARNING';
                        $host->disable();
                        \DBG::msg(
                            'SYNC ERROR: Host "' . $host->getHost() .
                                '" could not handle change #' .
                                $change->getId() . '. Host disabled.'
                        );
                        break 2;
                    }
                    $i++;
                    // remove host change
                    $change->removeHost($host);
                    $host->removeChange($change);
                    if (!$change->getHosts()->count()) {
                        // if change has no more hosts, remove change

                        // note: id mappings for delete operations can't be
                        // dropped until the delete operation has been pushed
                        // --> which is now
                        if ($change->getEventType() == 'delete') {
                            // this gets always triggered in case hack for Calendar is enabled
                            $entityClassName = $change->getSync()->getDataAccess()->getDataSource()->getIdentifier();
                            $mappings = $this->getIdMapping(
                                $entityClassName,
                                '',
                                '',
                                $change->getEntityIndexData(),
                                true
                            );
                            foreach ($mappings as $mapping) {
                                $em->remove($mapping);
                            }
                        }

                        $em->remove($change);
                    }
                }
                $host->removeLock();
            }
            $query = $em->createQuery('SELECT h FROM ' . $this->getNamespace() . '\Model\Entity\Host h WHERE h.state = 0');
            $query->useResultCache(false);
            $nonLockedHosts = $query->getResult();
            $hasNonLockedChanges = false;
            foreach ($nonLockedHosts as $host) {
                if (count($host->getChanges())) {
                    $hasNonLockedChanges = true;
                    break;
                }
            }
        }
        
        $logContents = ob_get_contents();
        ob_end_clean();
        echo $logContents;
        
        if ($severity == 'INFO' && $i == 0) {
            return;
        }
        
        $this->cx->getEvents()->triggerEvent('SysLog/Add', array(
           'severity' => $severity,
           'message' => 'Processed ' . $i . ' change(s)',
           'data' => $logContents,
        ));
    }
    
    /**
     * @todo: This method should be in DataSource model
     */
    public function getEntityIndexData($entity) {
        $em = $this->cx->getDb()->getEntityManager();
        $entityClassName = get_class($entity);
        if (substr($entityClassName, 0, 16) == 'Cx\Model\Proxies') {
            return $em->getUnitOfWork()->getEntityIdentifier($entity);
        }
        $entityMetaData = $em->getClassMetadata($entityClassName);
        $entityIndexData = array();
        foreach ($entityMetaData->getIdentifierFieldNames() as $field) {
            $entityIndexData[$field] = ''.$entityMetaData->getFieldValue($entity, $field);
        }
        return $entityIndexData;
    }
    
    /**
     * Returns the fields this entity depends on (relations that must be satisfied)
     * @todo: This method should be in DataSource model
     */
    public function getDependendingFields($entity) {
        $em = $this->cx->getDb()->getEntityManager();
        $entityClassName = get_class($entity);
        $entityMetaData = $em->getClassMetadata($entityClassName);
        $associationMappings = $entityMetaData->getAssociationMappings();
        $dependingFields = array();
        foreach ($associationMappings as $field => $associationMapping) {
            if (
                // @todo: we may use "TO_ONE" bitmask here (check newer doctrine versions and combined key support first)
                !in_array(
                    $associationMapping['type'],
                    array(
                        \Doctrine\ORM\Mapping\ClassMetadataInfo::ONE_TO_ONE,
                        \Doctrine\ORM\Mapping\ClassMetadataInfo::MANY_TO_ONE,
                    )
                )
            ) {
                continue;
            }
            $dependingFields[$field] = $associationMapping['targetEntity'];
        }
        return $dependingFields;
    }
    
    /**
     * Returns the fields another entity depends on (relations that should be satisfied)
     * @todo: This method should be in DataSource model
     */
    public function getCascadingFields($entity, $eventType, $manyToManyOnly = false) {
        $em = $this->cx->getDb()->getEntityManager();
        $entityClassName = get_class($entity);
        $entityMetaData = $em->getClassMetadata($entityClassName);
        $associationMappings = $entityMetaData->getAssociationMappings();
        $dependingFields = array();
        foreach ($associationMappings as $field => $associationMapping) {
            if (
                (
                    $eventType != 'delete' &&
                    !$associationMapping['isCascadePersist']
                ) ||
                (
                    $eventType == 'delete' &&
                    !$associationMapping['isCascadeRemove']
                )
            ) {
                continue;
            }
            if (
                $manyToManyOnly &&
                $associationMapping['type'] != \Doctrine\ORM\Mapping\ClassMetadata::MANY_TO_MANY
            ) {
                continue;
            }
            $dependingFields[$field] = $associationMapping['targetEntity'];
        }
        return $dependingFields;
    }
    
    /**
     * Remote side
     */
    public function getIdMapping($entityType, $foreignHost = '', $foreignId = '', $localId = '', $allowMultiple = false) {
        $em = $this->cx->getDb()->getEntityManager();
        $qb = $em->createQueryBuilder();
        $qb->select('m')
            ->from($this->getNamespace() . '\Model\Entity\IdMapping', 'm')
            ->andWhere($qb->expr()->eq('m.entityType', '?1'))
            ->setParameter(1, $entityType);
        if (!empty($foreignHost)) {
            $qb->andWhere($qb->expr()->eq('m.foreignHost', '?2'))
                ->setParameter(2, $foreignHost);
        }
        if (!empty($foreignId)) {
            $qb->andWhere($qb->expr()->like('m.foreignId', $qb->expr()->literal(serialize($foreignId))));
        }
        if (!empty($localId)) {
            $qb->andWhere($qb->expr()->like('m.localId', $qb->expr()->literal(serialize($localId))));
        }
        $mapping = null;
        $query = $qb->getQuery();
        $query->useResultCache(false);
        try {
            if ($allowMultiple) {
                $mapping = $query->getResult();
            } else {
                $mapping = $query->getSingleResult();
            }
        } catch (\Doctrine\ORM\NoResultException $e) {
        } catch (\Doctrine\ORM\NonUniqueResultException $e) {
            \DBG::log($query->getDql());
            throw $e;
        }
        return $mapping;
    }
    
    /**
     * Checks if there is an identifier ($identifier) of a remote
     * entity (of class $dataSourceIdentifier from host $foreignHost)
     * registered of which a local mapping exists. If so, then the
     * associated primary key entries in $fieldData will be translated
     * into the locally mapped identifier.
     * If $foreign is set to FALSE, then the check and replacement is
     * done in reverse, meaning that $identifier is threatened as the
     * local identifier and is translated into the remote
     * identifier from $foreignHost.
     *
     * @param   string  $foreignHost
     * @param   string  $dataSourceIdentifier
     * @param   array   $primaryKeyNames
     * @param   array   $fieldData
     * @param   array   $identifier
     * @param   boolean $foreign
     * @return  boolean Whether a mapping exists and has been replaced.
     */
    public function replacePrimaryKey(
        $foreignHost,
        $dataSourceIdentifier,
        $primaryKeyNames,
        &$fieldData,
        $identifier = array(),
        $foreign = true
    ) {
        $foreignId = '';
        $localId = '';
        if ($foreign) {
            $foreignId = $identifier;
        } else {
            $localId = $identifier;
        }
        $mapping = $this->getIdMapping(
            $dataSourceIdentifier,
            $foreignHost,
            $foreignId,
            $localId
        );

        // foreign entity already known locally
        if ($mapping) {
            if ($foreign) {
                $mappedIds = $mapping->getLocalId();
            } else {
                $mappedIds = $mapping->getForeignId();
            }
            foreach ($primaryKeyNames as $key) {
                $fieldData[$key] = $mappedIds[$key];
            }
            return true;
        }

        if (!$foreign) {
            return false;
        }

        // nullify auto-increment keys
        $em = $this->cx->getDb()->getEntityManager();
        $metaData = $em->getClassMetadata($dataSourceIdentifier);
        $associationMappingFields = array();
        $associationMappings = $metaData->getAssociationMappings();
        foreach ($associationMappings as $associationMapping) {
            // n:m associations can be skipped, as they are not handled by
            // a property of the processing entity
            if (
                $associationMapping['type'] ==
                \Doctrine\ORM\Mapping\ClassMetadata::MANY_TO_MANY
            ) {
                continue;
            }

            // inversed sides can be skipped, as those are not stored by the
            // processing entity (-> meaning there is no field that could be
            // set)
            if (!$associationMapping['isOwningSide']) {
                continue;
            }
            $associationMappingFields[$associationMapping['fieldName']] = $associationMapping;
        }
        foreach ($primaryKeyNames as $key) {
            if (!isset($associationMappingFields[$key])) {
                if ($this->useHackForCalendar($dataSourceIdentifier) && $key == 'langId') {
                    continue;
                }
                unset($fieldData[$key]);
                continue;
            }
            $foreignKey = $associationMappingFields[$key]['joinColumns'][0]['referencedColumnName'];
            $foreignData = [$foreignKey => $fieldData[$key]];
            $metaData = $em->getClassMetadata($associationMappingFields[$key]['targetEntity']);
            $this->replacePrimaryKey(
                $foreignHost,
                $associationMapping['targetEntity'],
                $metaData->getIdentifierFieldNames(),
                $foreignData,
                $foreignData
            );
            $fieldData[$key] = $foreignData[$foreignKey];
        }
        return true;
    }

    /**
     * @return boolean Whether we should use dodgy logic to make the sync for
     * component Calendar work.
     * @deprecated Will be dropped by CLX-3974
     */
    public function useHackForCalendar($entityClassName) {
        return strpos($entityClassName, 'Cx\\Modules\\Calendar\\Model\\Entity\\') === 0;
    }
}
