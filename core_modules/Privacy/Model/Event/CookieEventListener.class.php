<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * Class CookieEventListener
 *
 * @copyright   CLOUDREXX CMS - Cloudrexx AG Thun
 * @author      Michael Ritter <michael.ritter@cloudrexx.com>
 * @package     cloudrexx
 * @subpackage  coremodules_privacy
 */

namespace Cx\Core_Modules\Privacy\Model\Event;

/**
 * Class CookieEventListener
 *
 * @copyright   CLOUDREXX CMS - Cloudrexx AG Thun
 * @author      Michael Ritter <michael.ritter@cloudrexx.com>
 * @package     cloudrexx
 * @subpackage  coremodules_privacy
 */
class CookieEventListener implements \Cx\Core\Event\Model\Entity\EventListener {
    protected $isCookieUpdate = false;

    /**
     * @inheritdoc
     */
    public function onEvent($eventName, array $eventArgs) {
        $em = current($eventArgs)->getEntityManager();
        $uow = $em->getUnitOfWork();

        switch ($eventName) {
            case 'onFlush':
                // validate duplicate identifier
                $cookieRepo = $em->getRepository('Cx\Core_Modules\Privacy\Model\Entity\Cookie');
                foreach ($uow->getScheduledEntityInsertions() AS $entity) {
                    if (!is_a($entity, 'Cx\Core_Modules\Privacy\Model\Entity\Cookie')) {
                        continue;
                    }
                    $this->isCookieUpdate = true;
                    $this->checkCookieForDuplicateIdentifier($entity, $cookieRepo);
                }
                foreach ($uow->getScheduledEntityUpdates() AS $entity) {
                    if (!is_a($entity, 'Cx\Core_Modules\Privacy\Model\Entity\Cookie')) {
                        continue;
                    }
                    $this->isCookieUpdate = true;
                    $this->checkCookieForDuplicateIdentifier($entity, $cookieRepo);
                }
                break;
            case 'postFlush':
                if ($this->isCookieUpdate) {
                    // flush ESI cache
                    \Cx\Core\Core\Controller\Cx::instanciate()->getEvents()->triggerEvent(
                        'clearEsiCache',
                        array(
                            'Widget',
                            array(
                                'COOKIE_NOTE',
                            ),
                        )
                    );
                    $this->isCookieUpdate = false;
                }
                break;
        }
    }

    /**
     * Checks whether a Cookie's identifier is a duplicate
     * @param \Cx\Core_Modules\Privacy\Model\Entity\Cookie $entity Entity to check
     * @param \Doctrine\ORM\EntityRepository $cookieRepo Doctrine Cookie repository
     */
    protected function checkCookieForDuplicateIdentifier($entity, $cookieRepo) {
        global $_ARRAYLANG;

        $possibleDuplicate = $cookieRepo->findOneBy(array(
            'identifier' => $entity->getIdentifier(),
        ));
        if ($possibleDuplicate && $possibleDuplicate != $entity) {
            throw new \Cx\Core\Error\Model\Entity\ShinyException(
                sprintf(
                    $_ARRAYLANG['TXT_CORE_MODULES_PRIVACY_IDENTIFIER_ERROR_DUPLICATE'],
                    $entity->getIdentifier()
                )
            );
        }
    }
}

