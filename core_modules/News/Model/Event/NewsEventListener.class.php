<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * EventListener for News
 *
 * @copyright   Cloudrexx AG
 * @author      Project Team SS4U <info@cloudrexx.com>
 * @author      Thomas Wirz <thomas.wirz@cloudrexx.com>
 * @package     cloudrexx
 * @subpackage  coremodule_news
 */

namespace Cx\Core_Modules\News\Model\Event;

/**
 * EventListener for News
 *
 * @copyright   Cloudrexx AG
 * @author      Project Team SS4U <info@cloudrexx.com>
 * @author      Thomas Wirz <thomas.wirz@cloudrexx.com>
 * @package     cloudrexx
 * @subpackage  coremodule_news
 */
class NewsEventListener extends \Cx\Core\Event\Model\Entity\DefaultEventListener {

    /**
     * Global search event listener
     * Appends the News search results to the search object
     *
     * @param \Cx\Core_Modules\Search\Controller\Search $search
     */
    protected function SearchFindContent($search)
    {
        $newsLib = new \Cx\Core_Modules\News\Controller\NewsLibrary();
        $dbResult = $newsLib->getNewsFromDbForSearch($search);
        if (!$dbResult || $dbResult->EOF) {
            return;
        }
        $data = array();
        while (!$dbResult->EOF) {
            $data[] = array(
                'Score'     => $search->getPercentageFromScore(
                    $dbResult->fields['score']
                ),
                'Title'     => $dbResult->fields['title'],
                'Content'   => $search->parseContentForResultDescription(
                    $dbResult->fields['content']
                ),
                'Link'      => $newsLib->getApplicationUrl($dbResult->fields),
                'Date'      => $dbResult->fields['date'] ?? null,
                'Component' => 'News',
            );
            $dbResult->MoveNext();
        }
        $result = new \Cx\Core_Modules\Listing\Model\Entity\DataSet($data);
        $search->appendResult($result);
    }
}
