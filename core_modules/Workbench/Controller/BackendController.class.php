<?php declare(strict_types=1);

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * BackendController for Workbench
 *
 * @author Michael Ritter <michael.ritter@cloudrexx.com>
 */

namespace Cx\Core_Modules\Workbench\Controller;

/**
 * BackendController for Workbench
 *
 * @author Michael Ritter <michael.ritter@cloudrexx.com>
 */
class BackendController extends \Cx\Core\Core\Model\Entity\SystemComponentBackendController {

    /**
     * @override
     */
    public function getCommands() {
        return array('sandbox'=>array('php'), 'development'=>array('components'));
    }

    /**
     * @override
     */
    public function parsePage(\Cx\Core\Html\Sigma $template, array $cmd, &$isSingle = false) {
        global $_ARRAYLANG;

        if (empty($cmd[1])) {
            if ($cmd[0] == 'sandbox') {
                $cmd[1] = 'dql';
            } else {
                $cmd[1] = 'yaml';
            }
        }
        if ($cmd[0] == 'development') {
            $cmd[0] = 'toolbox';
        }
        $controller = ucfirst($cmd[0]);
        $this->getController($controller)->parsePage(
            $_ARRAYLANG,
            $cmd[1],
            $_POST
        );
        $template->setVariable('ADMIN_CONTENT', $this->getController($controller));
    }

    /**
     * @override
     */
    public function showOverviewPage() {
        return false;
    }
}

