<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * ComponentController for Workbench
 *
 * Loads backend view Controllers and adds warning message
 * @author Michael Ritter <michael.ritter@comvation.com>
 */

namespace Cx\Core_Modules\Workbench\Controller;

/**
 * ComponentController for Workbench
 *
 * Loads backend view Controllers and adds warning message
 * @author Michael Ritter <michael.ritter@comvation.com>
 */
class ComponentController extends \Cx\Core\Core\Model\Entity\SystemComponentController {
    /**
     * getControllerClasses
     *
     * @return type
     */
    public function getControllerClasses() {
        return array('Backend', 'Sandbox', 'Toolbox');
    }

    public function getCommandsForCommandMode() {
        $cliOnlyPermission = new \Cx\Core_Modules\Access\Model\Entity\Permission(
            array(),
            array('cli'),
            false
        );
        return array(
            'workbench' => $cliOnlyPermission,
            'wb' => $cliOnlyPermission,
        );
    }

    public function getCommandDescription($command, $short = false) {
        switch ($command) {
            case 'workbench':
                return 'Development framework';
            case 'wb':
                return 'Shortcut alias for `workbench`';
        }
    }

    public function executeCommand($command, $arguments, $dataArguments = array())
    {
        new \Cx\Core_Modules\Workbench\Model\Entity\ConsoleInterface(array_merge(array($command), $arguments), $this->cx);
    }

    /**
     * Add the warning banner
     *
     * @param \Cx\Core\ContentManager\Model\Entity\Page $page Resolved page
     */
    public function postContentLoad(\Cx\Core\ContentManager\Model\Entity\Page $page) {
        $objTemplate = $this->cx->getTemplate();
        $warning = new \Cx\Core\Html\Sigma(ASCMS_CORE_MODULE_PATH . '/Workbench/View/Template/Backend');
        $warning->loadTemplateFile('Warning.html');
        if (!in_array(
            $this->cx->getMode(),
            array(
                \Cx\Core\Core\Controller\Cx::MODE_FRONTEND,
                \Cx\Core\Core\Controller\Cx::MODE_BACKEND,
            )
        )) {
            return;
        }
        \JS::registerCSS('core_modules/Workbench/View/Style/Warning' . ucfirst($this->cx->getMode()) . '.css');
        $objTemplate->_blocks['__global__'] = preg_replace('/<body[^>]*>/', '\\0' . $warning->get(), $objTemplate->_blocks['__global__']);
        if ($this->cx->getMode() == \Cx\Core\Core\Controller\Cx::MODE_FRONTEND) {
            \JS::registerJS('core_modules/Workbench/View/Script/Warning.js');
        }
    }
}
